public class KingChargoth extends Monster 
{
	private GoldenKey goldenKey;

	public KingChargoth()
	{
		super("King Chargoth", 150, 200, 0);

		this.goldenKey = new GoldenKey();
	}

	public GoldenKey getGoldenKey()
	{
		return this.goldenKey;
	}
}