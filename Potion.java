public abstract class Potion extends Item
{
	private int potionAmount;

	public Potion(String name, int potionAmount)
	{
		super(name);

		this.potionAmount = potionAmount;
	}

	public int getPotionAmount()
	{
		return this.potionAmount;
	}
}