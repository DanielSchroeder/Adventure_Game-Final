public abstract class GameCharacter
{
	private String name;
	private int    attackPower;
	private int    health;
	private int    xP; //monsters will give xP to players allowing them to level up
	private Weapon weapon;
	private HealthPotion healthPotion;
	private ManaPotion manaPotion;

	//these are non-static because both Monster and Player classes should have their
	//own version of these counters.
	private int healthPotionCount = 0;
	private int manaPotionCount = 0;
	private int weaponCount = 0;

	public GameCharacter(String name, int attackPower, int health, int xP)
	{
		this.name = name;
		this.attackPower = attackPower;
		this.health = health;
		this.xP = xP;
	}


	//will be implemented by child classes Player and Monster in different ways.
	//Player will be able to make decisions on whether to use potions or attack.
	//Monster will just attack.
	public abstract int takeTurn();

	//will be called by takeTurn in Player and Monster child classes.
	public abstract void displayStats();

	public String getName()
	{
		return this.name;
	}
	
	public int getXP()
	{
		return this.xP;
	}

	public void setXP(int newXP)
	{
		this.xP = newXP;
	}

	public void updateXP(int newXP)
	{
		this.xP += newXP;
	}

	public int getAttackPower()
	{
		return this.attackPower;
	}

	public void updateAttackPower(int newAttack)
	{
		this.attackPower += newAttack;
	}

	public int getHealth()
	{
		return this.health;
	}

	//used in the App class lastBreath to set the hero's health to 
	//0 before taking a health potion
	public void setHealth(int newHealth)
	{
		this.health = newHealth;
	}

	public boolean isAlive()
	{
		return (getHealth() > 0 ? true : false);
	}

	public void updateHealth(int newHealth)
	{
		this.health += newHealth;
	}

	public boolean hasItems()
	{
		return (hasPotion() || hasWeapon() ? true : false);
	}

	//will be overridden in Player class to allow the player to discard the 
	//current weapon for a new one if they so choose. Since a player or monster
	//can only have one weapon, the weapon count is always either 0 or 1.
	public void setWeapon(Weapon weapon)
	{
		this.weapon = weapon;
		weaponCount = 1;
	}

	public void decreaseWeaponCount()
	{
		weaponCount--;
	}

	public Weapon getWeapon()
	{
		return this.weapon;
	}

	public boolean hasWeapon()
	{
		return (weaponCount > 0 ? true : false);
	}

	public void setHealthPotion(HealthPotion healthPotion)
	{
		this.healthPotion = healthPotion;
		healthPotionCount++;
	}

	public void decreaseHealthPotionCount()
	{
		healthPotionCount--;
	}

	public HealthPotion getHealthPotion()
	{
		return this.healthPotion;
	}

	public int getHealthPotionCount()
	{
		return this.healthPotionCount;
	}

	public boolean hasHealthPotion()
	{
		return (healthPotionCount > 0 ? true : false);
	}

	public void setManaPotion(ManaPotion manaPotion)
	{
		this.manaPotion = manaPotion;
		manaPotionCount++;
	}

	public void decreaseManaPotionCount()
	{
		manaPotionCount--;
	}

	public ManaPotion getManaPotion()
	{
		return this.manaPotion;
	}

	public int getManaPotionCount()
	{
		return this.manaPotionCount;
	}

	public boolean hasManaPotion()
	{
		return (manaPotionCount > 0 ? true : false);
	}

	public boolean hasPotion()
	{
		return (hasManaPotion() || hasHealthPotion() ? true : false);
	}
}