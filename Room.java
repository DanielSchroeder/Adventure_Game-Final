public class Room 
{
	private String description;
	private Room north;
	private Room east;
	private Room west;
	private Room south;
	private Weapon weapon;
	private HealthPotion healthPotion;
	private ManaPotion manaPotion;

	//will be used to set the room that King Chargoth is in
	private boolean lastBattleRoom;

	public Room(String description, boolean lastBattleRoom)
	{
		this.description = description;
		this.lastBattleRoom = lastBattleRoom;
	}
	
	//***CAREFUL, SOME OF THESE WILL BE SET TO NULL SINCE SOME DIRECTIONS
	//DON'T HAVE EXITS IN EACH ROOM***
	public void setExits(Room north, Room east, Room west, Room south)
	{
		this.north = north;
		this.east = east;
		this.west = west;
		this.south = south;
	}

	public String getDescription()
	{
		return this.description;
	}

	//will be used as the conditional in an if statement in the app that checks
	//to see if this is the room that King Chargoth is in. 
	public boolean isLastBattleRoom()
	{
		return this.lastBattleRoom;
	}

	//These methods ensure that you don't get a null pointer exception
	//by checking for a null value and handling it by returning a boolean.
	//These methods are called in getExits to construct a string description
	//of the exits each room possesses. Also called from the app's executeChoice()
	//to determine whether the user's choice was valid.
	public boolean possibleExitNorth()
	{
		return (this.north == null ? false : true);
	}

	public boolean possibleExitEast()
	{
		return (this.east == null ? false : true);
	}

	public boolean possibleExitWest()
	{
		return (this.west == null ? false : true);
	}

	public boolean possibleExitSouth()
	{
		return (this.south == null ? false : true);
	}

	//method checks to see if possibleExit______(direction)
	//evalutes to true. If so, it adds a string of the direction
	//to the exitDescriptions string. This method gets returned to
	//the app in the toString() method.
	public String getExits()
	{
		String exitDescriptions = "There are exits to the";

		if (possibleExitNorth())
		{
			exitDescriptions += " |NORTH|";
		}	

		if (possibleExitEast())
		{
			exitDescriptions += " |EAST|";
		}	

		if (possibleExitWest())
		{
			exitDescriptions += " |WEST|";
		}
		
		if (possibleExitSouth())
		{
			exitDescriptions += " |SOUTH|";
		}	

		return exitDescriptions;
	}

	//methods will return the Room object to the app in the
	//executeChoice method. This object will become the new
	//currentRoom in the app
	public Room getNorthRoom()
	{
		return this.north;
	}

	public Room getEastRoom()
	{
		return this.east;
	}

	public Room getWestRoom()
	{
		return this.west;
	}

	public Room getSouthRoom()
	{
		return this.south;
	}

	public Weapon getWeapon()
	{
		return this.weapon;
	}

	public void setWeapon(Weapon weapon)
	{
		this.weapon = weapon;
	}

	public boolean hasWeapon()
	{
		return (this.weapon == null ? false : true);
	}

	public void setHealthPotion(HealthPotion healthPotion)
	{
		this.healthPotion = healthPotion;
	}

	public HealthPotion getHealthPotion()
	{
		return this.healthPotion;
	}

	public boolean hasHealthPotion()
	{
		return (this.healthPotion == null ? false : true);
	}

	public void setManaPotion(ManaPotion manaPotion)
	{
		this.manaPotion = manaPotion;
	}

	public ManaPotion getManaPotion()
	{
		return this.manaPotion;
	}

	public boolean hasManaPotion()
	{
		return (this.manaPotion == null ? false : true);
	}

	public boolean hasPotion()
	{
		return (this.healthPotion == null && this.manaPotion == null ? false : true);
	}

	public boolean hasItems()
	{
		return (hasPotion() || hasWeapon() ? true : false);
	}

	//called after the player exits a room to clear the room of items
	public void clearItems()
	{
		this.manaPotion = null;
		this.healthPotion = null;
		this.weapon = null;
	}
	
	//returns a formatted string of the room's description and
	//the exits from the room.
	@Override
	public String toString()
	{
		return String.format("%s%n", getDescription());
	}
}