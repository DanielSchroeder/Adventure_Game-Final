import java.util.Scanner;

public class Player extends GameCharacter
{
	//inherits name, attackPower, health and xP from GameCharacter
	private int mana;
	private int level;
	private int magicAttack; //strength of the players magic attacks
	private GoldenKey goldenKey;

	public static Scanner input = new Scanner(System.in);
	
	//player wll get to select each field from a prompt.
	public Player(String name, int attackPower, int health, int xp, int mana)
	{
		super(name, attackPower, health, xp); 

		this.mana = mana;
		this.level = 1; 
		this.magicAttack = 18;
	}

	//All this does is get the user's choice for which action to take
	//the app class handles calling the methods on this choice.
	//I did this becuause some of the choices affect the player and 
	//some affect the monster.
	@Override
	public int takeTurn()
	{
		displayStats();

		displayItems();

		//prompt user for action to take against a monster
		//can allow the user to attack or use any potions
		//gathered thus far to increase health and mana.
		int userChoice = prompt();

		return userChoice;
	}

	public void setGoldenKey(GoldenKey goldenKey)
	{
		this.goldenKey = goldenKey;
	}

	public boolean hasGoldenKey()
	{
		return (goldenKey != null ? true : false);
	}

	//called by takeTurn(), allows the player to choose from three options
	//returns it's choice to the App's heroTurn() where it is processed.
	public int prompt()
	{
		System.out.printf("Choose one of the following options:%n" +
			"1. Attack%n" +
			"2. Use Mana Potion (+5 mana)%n" +
			"3. Use Health Potion (+15 health)%n" +
			"4. Cast Attack Spell (5 mana)%n" +
			"5. Cast Healing Spell (10 mana for +30 health)%n");
		int userChoice = input.nextInt();
		input.nextLine();//consumes the newline character

		return userChoice;
	}

	@Override
	public void displayStats()
	{
		System.out.printf("%n*************************%n" +
			"%n%s's Stats:%n%n" +
			"Hero Health: %d%n" +
			"Hero Attack Power: %d%n" +
			"Hero Magic Attack Power: %d%n" +
			"Hero Mana: %d%n" +
			"Hero xP: %d%n" +
			"Hero level: %d%n" +
			"%n*************************%n",
			getName(), getHealth(), attack(), getMagicAttack(), 
			getMana(), getXP(), getLevel());
	}

	public void displayItems()
	{
		System.out.printf("%n^^^^^^^^^^^^^^^^^^^^^^^^^%n" +
			"%nAvailable Items:%n%n" +
			"Current Weapon: %s%n" +
			"Health Potion(s): %d (+15 health)%n" +
			"Mana Potion(s): %d (+5 mana)%n" +
			"%n^^^^^^^^^^^^^^^^^^^^^^^^^%n%n",
			(hasWeapon() == true ? getWeapon().getName() : "No Weapon"),
			getHealthPotionCount(),
			getManaPotionCount());
	}

	//allows the player to choose wheter to enter combat or not. Called by the App's
	//runCombatLoop().
	public boolean enterCombat()
	{
		boolean returnValue = true;

		System.out.print("Choose 1 to enter combat or 0 to run away: ");
		int userInput = input.nextInt();
		input.nextLine();//consumes the newline character

		System.out.println();

		if (userInput == 1)
		{
			returnValue = true;
		}	
		else if (userInput == 0)
		{
			returnValue = false;
		}	
		else
		{
			System.out.println("Invalid choice, so you're going to have to fight!");
			returnValue = true;
		}

		return returnValue;

	}

	//determines whether or not the player has leveled up after each round
	//of combat. Called by the App's heroVictory()
	public void levelUp()
	{
		//condition will make it so each level takes progressivley more xP to level
		//up. ALso, setting the xP back to 0 after each level up is essential.
		if(getXP() >= 10 * getLevel())
		{
			//level up one level
			this.level++;

			//set the xP to 0
			setXP(0);

			//increase health
			updateHealth(2 * getLevel() + 20);

			//increase attackPower
			updateAttackPower(getLevel());

			//increase magicAttack
			updateMagicAttack(5 + getLevel());

			//increase mana
			updateMana(1 * getLevel());

			System.out.println("Congratulations, you have leveled up!");

			displayStats();

		}	
	}

	public int getLevel()
	{
		return this.level;
	}

	public int getMana()
	{
		return this.mana;
	}

	public int getMagicAttack()
	{
		return this.magicAttack;
	}

	//called in this class by levelUp()
	public void updateMagicAttack(int newMagicAttack)
	{
		this.magicAttack += newMagicAttack;
	}

	//called by castSpell and useManaPotion in this class
	public void updateMana(int newManaAmount)
	{
		this.mana += newManaAmount;

		//validates that the mana will always be 0 or greater
		if (getMana() < 0)
		{
			this.mana = 0;
		}	
	}

	public void useHealthPotion()
	{
		if (hasHealthPotion())
		{
			updateHealth(getHealthPotion().getPotionAmount());
			decreaseHealthPotionCount(); //allows for more than one potion

			System.out.printf("Health increased by %d%n",
				getHealthPotion().getPotionAmount());
		}	
		else
		{
			System.out.println("You don't have a health potion!");
		}
	}

	public void useManaPotion()
	{
		if (hasManaPotion())
		{
			updateMana(getManaPotion().getPotionAmount());
			decreaseManaPotionCount(); //allows for more than one potion
		}
		else
		{
			System.out.println("You don't have a mana potion!");
		}	
	}
	
	//allows the user to choose whether to take the new weapon or keep their
	//current weapon(if they have one).
	@Override
	public void setWeapon(Weapon weapon)
	{
		if (hasWeapon())
		{
			System.out.printf("%nYou currently have a %s with attack power %d%n" +
				"Do you want to exchange it for this %s with attack power %d?%n" +
				"Enter y/n: ",
				getWeapon().getName(), getWeapon().getAttackDamage(),
				weapon.getName(), weapon.getAttackDamage());
			String userChoice = input.next();
			input.nextLine();//consumes the newline character

			System.out.println();

			//if the user has a weapon already and wants to equip the new weapon,
			//check to see if they are leveled-up enough to possess certain weapons
			if (userChoice.equals("y"))
			{
				weaponCheck(weapon);	
			}
		}
		else
		{
			weaponCheck(weapon);
		}	
	}

	private void weaponCheck(Weapon weapon)
	{
		if (weapon instanceof BattleAxe)
		{
			if (getLevel() >= 7)
			{
				super.setWeapon(weapon);
			}
			else
			{
				System.out.println("You must be at least at level 7 " +
					"to have a BattleAxe!");
			}	
		}
		else if (weapon instanceof BroadSword)
		{
			if (getLevel() >= 5)
			{
				super.setWeapon(weapon);
			}	
			else
			{
				System.out.println("You must be at least at level 5 " +
					"to have a BroadSword!");
			}	
		}
		else if (weapon instanceof CrossBow)
		{
			if (getLevel() >= 3)
			{
				super.setWeapon(weapon);
			}	
			else
			{
				System.out.println("You must be at least at level 3 " +
					"to have a CrossBow!");
			}	
		}	
		else
		{
			super.setWeapon(weapon);
		}
	}

	//called by heroTurn() in the App. Allows the hero to utilize the extra
	//strength of their current weapon. If they don't have a weapon, the default
	//dagger strength is used.
	public int attack()
	{
		int attackPower;

		if (hasWeapon())
		{
			attackPower = getAttackPower() + getWeapon().getAttackDamage();
		}	
		else
		{
			attackPower = getAttackPower();
		}

		return attackPower;
	}

	//called by heroTurn in the App class. The App does the same if check, but 
	//I figure this extra layer of protection couldn't hurt in case I want to use
	//this method down the line somewhere else in the App without having to bother
	//with the if check in the App.
	public int castAttackSpell()
	{
		int spellPower;

		if (getMana() >= 5)
		{
			spellPower = getMagicAttack();

			updateMana(-3);
		}	
		else
		{
			spellPower = 0;
		}

		return spellPower;
	}

	public void castHealthSpell()
	{
		if (getMana() >= 10)
		{
			this.updateHealth(30);
		}	
		else
		{
			System.out.println("You don't have enough mana!");
		}
	}
	

}