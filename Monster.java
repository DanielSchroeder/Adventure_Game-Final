import java.util.Scanner;

public abstract class Monster extends GameCharacter
{
	//inherits name, attackPower, health and xP from GameCharacter

	public static Scanner input = new Scanner(System.in);

	public Monster(String name, int attackPower, int health, int xP)
	{
		super(name, attackPower, health, xP);
	}

	//in the monster class, this returns the attack damage 
	@Override
	public int takeTurn()
	{
		//print monster's stats
		displayStats();

		System.out.printf("The %s attacks you for %d damage.%n%n",
			getName(), getAttackPower());

		//return the negative value of the Monster's attack power
		return -getAttackPower();
	}

	//displays the instance variables of each monster
	@Override
	public void displayStats()
	{
		System.out.printf("%n:::::::::::::::::::::::::%n" + 
			"%n%s's Stats:%n%n" +
			"Monster Health: %d%n" + 
			"Monster Attack Power: %d%n" +
			"Monster xP: %d%n" +
			"%n:::::::::::::::::::::::::%n%n",
			getName(), getHealth(), getAttackPower(), getXP());
	}

}