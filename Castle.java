public class Castle
{
	//instance variables for rooms of the castle
	private Room mainEntrance; 
    private Room southAntechamber;
    private Room eastHall;
    private Room atrium;
    private Room kingBedroom; 
    private Room queenBedroom;
    private Room westHall;
    private Room northAntechamber; 
    private Room armory;
    private Room watchtower;
    private Room diningRoom; 
    private Room kitchen;

    //constructor instantiates Room object's with descriptions and sets exits.
    public Castle()
    {
    	this.mainEntrance = new Room("You are in the main entrance. " +
    		"It is magnificent! The walls are lined " +
            "with the finest\nmarble from the quarries at Roquefort. " +
            "Chargoth's royal coat of arms is displayed on a\ntapestry " +
            "of gold and purple silk running the length of the west wall. " +
            "You breathe in the\naromatic scent of a roasting meat coming from " +
            "somewhere in the depths of the castle.\n", false);

    	this.southAntechamber = new Room("You are in the south antechamber. " +
    		"There are hallways on either side of you.\n", false);

    	this.eastHall = new Room("You are in the east hall. " +
    		"It is dimly lit by torches and a sliver of " +
            "light coming through\nthe barred windows. There are decorative " +
            "cornices running the length of the hall on either\nside.\n", false);
    	
    	this.atrium = new Room("You are in the atrium. " +
    		"A fountain gushes forth in the center of the " +
            "courtyard. There are\nwindows on all of the walls, and the wooden " +
            "ceiling can be opened by operating a lever on\nthe south wall.\n", false);

    	this.kingBedroom = new Room("You are in the king's bedroom. " +
    		"You smell a faint odor of cedar and woodsmoke. " +
            "A large\nbroadsword lies next to his enormous bed, and his " +
            "windows overlook the Cheshire valley\nbelow. " +
            "His ruby-encrusted bronze helmet lies atop a coffer draped " +
            "with the skin of some\nexotic beast.\n", false);

    	this.queenBedroom = new Room("You are in the queen's bedroom. " +
    		"The light from the open balcony door is " +
            "blinding! A\nfaint breeze blows the curtains into the room. " +
            "You notice a small library and " +
            "books lying\nopen on alomst every surface. " +
            "In the corner of the room rests a small easel with an\n" +
            "unfinised portrait of a handsom rider atop a black Persian " +
            "steed resting against the base.\n", false);

    	this.westHall = new Room("You are in the west hall. " +
    		"There is a small staircase at the north end " +
            "of the hall.\nPortraits of the regency hang along the west wall, " +
            "while the south wall has various\nweapons of war arranged on " +
            "ivory racks suspended from the ceiling.\n", false);

    	this.northAntechamber = new Room("You are in the north antechamber. " +
    		"There a several couches and chairs scattered " +
            "about the\nroom. A lute lies precariously near the edge of a low " +
            "table. Several decanters with\nintoxicating fumes lie mostly empty " +
            "on small end-tables. Festive voices can be heard to\nthe north, " +
            "and to the west you can hear the sound of soldiers arguing.\n", false);

    	this.armory = new Room("You are in the armory. " +
    		"The south wall holds an assortment of wearable " +
            "armor, while the\nnorth wall is cluttered with every concievable " +
            "death-giving device.\n", false);

    	this.watchtower = new Room("You are in the watchtower. " +
    		"You step up onto the battlements and encounter " +
            "several drunk\nsoldiers who are arguing about the " +
            "merits of a recently deceased comrade. " +
            "They seem to be\nmore concerned about the wherabouts of his " +
            "beautiful black horse than revenging his death.\n" +
            "You can see every corner of the king's hamlet from this " +
            "vantage point, and the view is marvelous.\n", false);

    	this.diningRoom = new Room("You are in the dining room. " +
    		"A roasted pig sits atop a bed of fresh wild " +
            "greens. There\nare many people dining at a giant table that " +
            "spans the length of the east wall. The king\nis seated on a " +
            "gold throne feasting on the haunches of the pig. " +
            "Revelry and merriment\nabound, but the queen is nowhere in sight.\n", true);

    	this.kitchen = new Room("You are in the kitchen. " +
    		"There are a handful of servants tending to a " +
            "cast iron cauldron\nhung above a fire pit. " +
            "Wild mushrooms, herbs, fowl, and rabbit lie scattered on a " +
            "table\nnext to an assortment of sterling silver utensils. " +
            "A single window to the north looks out\nto the private garden " +
            "below.\n", false);


    	//set the exits of each room
    	//***I TRIED TO DO THIS RIGHT AFTER EACH DESCRIPTION BUT RECIEVED 
    	//NULL-POINTER EXCEPTIONS IN THE APP. I THINK THIS WAS BECAUSE
    	//THE ROOMS THAT I WAS TRYING TO REFERENCE HADN'T BEEN CREATED
    	//WITH THE NEW KEYWORD YET. IT WAS PRETTY FRUSTRATING, DON'T FORGET
    	//THAT THE OBJECT DOESN'T EXIST UNTIL THAT NEW KEYWORD, SO IF YOU
    	//TRY TO ACCESS IT(EVEN IN THE SAME BLOCK!) BEFORE IT HAS BEEN CREATED,
    	//YOU WILL GET A NULL REFERENCE!!!***
    	this.mainEntrance.setExits(this.southAntechamber, null, null, null);
    	this.southAntechamber.setExits(null, this.eastHall, this.westHall, null);
    	this.eastHall.setExits(this.queenBedroom, this.atrium, this.southAntechamber,
    		null);
    	this.atrium.setExits(this.kingBedroom, null, this.eastHall, null);
    	this.kingBedroom.setExits(null, null, this.queenBedroom, this.atrium);
    	this.queenBedroom.setExits(null, this.kingBedroom, null, this.eastHall);
    	this.westHall.setExits(this.northAntechamber, this.southAntechamber, null, 
    		null);
    	this.northAntechamber.setExits(this.diningRoom, null, this.armory,
    		this.westHall);
    	this.armory.setExits(null, this.northAntechamber, this.watchtower, null);
    	this.watchtower.setExits(null, this.armory, null, null);
    	this.diningRoom.setExits(this.kitchen, null, null, this.northAntechamber);
    	this.kitchen.setExits(null, null, null, this.diningRoom);

    }//end of Castle constructor

    public Room startingRoom()
    {
    	return this.mainEntrance;
    }

}//end Castle class