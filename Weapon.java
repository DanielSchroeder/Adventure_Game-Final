public abstract class Weapon extends Item
{
	private int attackDamage;

	public Weapon(String name, int attackDamage)
	{
		super(name);

		this.attackDamage = attackDamage;
	}

	public int getAttackDamage()
	{
		return this.attackDamage;
	}
}