/*

Ideas for possible updates to this program:

1. Allow the user to interact with objects or persons in the current room
    -Examples would be the cauldron in the kitchen or the lever that
    operates the ceiling in the atrium.
2. Create a "dungeon" that lies through a trap door in one of the 
    rooms. If the user falls in, they have to fight some monster or figure
    out some puzzle in order to escape.
        -The puzzle could be a math riddle, you could randomize it with
        a random number generator.
    You could also create a randomized array that models a labyrinth of some
    sort that the user has to navigate. 
        -Within this labyrinth there could be riddles and monsters to fight.
3. Create more "stock" descriptions of each room, maybe these could change
   depending on which room the user just left or at what stage of the game
   the user is in.
4. Make the program more fault-tolerant through exception handling mechanisms.
5. Create more items
6. Have the monster's attack damage more than just the user's health
7. Randomize the amount of damage that the monster or player can inflict
8. Create many more monsters, potions, and weapons.
   
*/

import java.util.Scanner;
import java.security.SecureRandom;

public class Adventure_Game_Final
{
	//static class variables. Since we have static methods
	//these must be static as well so the methods can access
	//them
	public static Scanner input = new Scanner(System.in);
    public static SecureRandom randomGenerator = new SecureRandom();

    public enum Status {WIN, LOSE, CONTINUE, QUIT, DEAD};
    //used as the loop-continuation condition in the game loop
	public static Status gameStatus = Status.CONTINUE;
	//used as the loop-continuation condition in the combat loop
	public static boolean combatStatus = true;

	public static void main(String[] args)
	{
		//create a new instance of Castle
		Castle chargothCastle = new Castle();

		//will hold the current room during the game loop
		//init to starting room in the castle
		Room currentRoom = chargothCastle.startingRoom();

		drawScreen();

        displayOpeningMessage();

        pressEnterToContinue();

        drawScreen();

        //call the playerCreationPrompt() method to create a user-defined player 
        //return this user-defined player to the hero player
        Player hero = playerCreationPrompt();

        //this is labeled for the labeled break statement in the lastBattle
        //portion of the game loop.
        gameLoop:
    		while (gameStatus == Status.CONTINUE)
    		{		
                drawScreen();

    			//print the current room (will be starting room in first case)
                System.out.println("$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$\n");
    			System.out.print(currentRoom);
                System.out.println("$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$\n\n");

                //if this room is the dining room where King Chargoth resides, 
                //see if the user is ready to fight him.
                if (currentRoom.isLastBattleRoom())
                {
                    lastBattle(hero);

                    //if the hero is dead after battling Chargoth, set the  
                    //gameStatus to DEAD and break from the gameLoop
                    if (!hero.isAlive())
                    {
                        gameStatus = Status.DEAD;
                        break gameLoop;
                    }

                    //if the hero has the golden key that means he has defeated
                    //King Chargoth, so break the gameLoop but leave the gameStatus
                    //alone, as it will control the queenRescue portion below
                    if (hero.hasGoldenKey())
                    {
                        break gameLoop;
                    }   
                }   

    			//possbily fill the room with items (can be potions or weapons)
    			fillWithItems(currentRoom);

    			//if the room has items, allow the user to get them
    			equipItems(hero, currentRoom);

    			//clear the room of any items so that it won't contain the same items
    			//if the user goes in there again
    			currentRoom.clearItems();

    			//list the exits from the room
    			System.out.printf("%n%s%n", currentRoom.getExits());

    			//prompt the user for a direction of travel or quit
                String userChoice = travelPrompt();
                
                //if user enters q, break out of the loop
                if (userChoice.equals("q"))
                {  
                    gameStatus = Status.QUIT;
                    break;
                }
                else
                {
                    //method that proccesses user's choice, checks for validity,
                    //and if valid changes currentRoom to the room in the direction
                    //of the user's choice
                    currentRoom = executeChoice(currentRoom, userChoice);
                }

                //Creates a random monster and
                //runs a combat loop with the player and that monster.
                if (1 + randomGenerator.nextInt(3) == 2)
                {
                    runCombatLoop(hero);
                }    
                
    		} //end game loop

        //if statement allows the hero to rescue the princess if they 
        //have the golden key and the gameStatus is still continue
        if (gameStatus == Status.CONTINUE && hero.hasGoldenKey())
        {
            rescueQueen();
        }    

		//method to process the final results of the game
		System.out.println("****************************************GAME RESULT****************************************");
        displayGameResult();
        System.out.println("\n*******************************************************************************************");

	}//end main method

	//method that prompts the user for their direction of travel
    public static String travelPrompt()
    {
        //prompt the user for their direction of travel
        System.out.println("\nWhich direction would you like to travel?\n" +
        "Enter n for North\n" +
        "Enter e for East\n" +
        "Enter w for West\n" +
        "Enter s for South\n" +
        "Enter q to quit");
        System.out.println("----------------------------------------");
        String userChoice = input.next();
        input.nextLine();//consumes the newline character
        
        //for formatting
        System.out.println();
        
        return userChoice;

    }//end travelPrompt method

    //method that proccesses user's choice, checks for validity,
    //and if valid moves the user to the new room.
    //***WOULD IT BE BETTER TO CREATE SOME UTILITY (HELPER) METHODS THAT
    //THIS METHOD CAN CALL ON TO DO SOME OF THE WORK INSTEAD OF JUST CODING
    //EVERYTHING IN ONE METHOD???***
    //rewrote this method several times, think I got it now. When we pass
    //the currentRoom object to this method, Java makes a shallow-copy of 
    //it and modifies the copy (NOT THE ORIGINAL VALUE!). Because of this,
    //we need to return the reference (returnRoom) to the calling method
    //and store it in the currentRoom variable in main.
    public static Room executeChoice(Room currentRoom, String userChoice)
    {
        Room returnRoom = currentRoom;

        //check the user's choice
    	if (userChoice.equals("n"))
    	{
    		//if the user's choice is north, see if that direction is possible
    		if (currentRoom.possibleExitNorth())
    		{
    			//if they can travel that direction, set currentRoom to the value
    			//of the north room
    			returnRoom = currentRoom.getNorthRoom();
    		}	
    		else
    		{
    			System.out.println("Invalid choice!");

    			pressEnterToContinue();
    		}
    	}

    	if (userChoice.equals("e"))
    	{
    		if (currentRoom.possibleExitEast())
    		{
    			returnRoom = currentRoom.getEastRoom();
    		}	
    		else
    		{
    			System.out.println("Invalid choice!");

    			pressEnterToContinue();
    		}
    	}

    	if (userChoice.equals("w"))
    	{
    		if (currentRoom.possibleExitWest())
    		{
    			returnRoom = currentRoom.getWestRoom();
    		}	
    		else
    		{
    			System.out.println("Invalid choice!");

    			pressEnterToContinue();
    		}
    	}

    	if (userChoice.equals("s"))
    	{
    		if (currentRoom.possibleExitSouth())
    		{
    			returnRoom = currentRoom.getSouthRoom();
    		}	
    		else
    		{
    			System.out.println("Invalid choice!");

    			pressEnterToContinue();
    		}
    	}

        return returnRoom;

    }//end executeChoice method

    //allows user to choose the player object's stats
    public static Player playerCreationPrompt()
    {
        int    heroAttack = 0;
        int    heroHealth = 0;
        int    heroMana = 0;
        int    statPoints = 10; 

        System.out.println("Enter a name for the hero: ");
        String heroName = input.next();
        input.nextLine();

        while (statPoints > 0)
        {
            System.out.printf("%nStat-points remaining: %d%n%n" +
                "Choose which stat to update: %n" +
                "1. Attack Power + 5%n" +
                "2. Health + 5%n" +
                "3. Mana + 3%n",
                statPoints);
            int userChoice = input.nextInt();
            input.nextLine();//consumes the newline character

            if (userChoice == 1)
            {
                heroAttack += 5;
            }    
            else if (userChoice == 2)
            {
                heroHealth += 5;
            } 
            else if (userChoice == 3)
            {
                heroMana += 3;
            }
            else //userChoice invalid
            {
                System.out.println("Invalid choice!");
                statPoints++;
            }

            statPoints--;
        }

        if (heroHealth == 0)
        {
            System.out.println("Your health is 0, so you don't get to play!");

            pressEnterToContinue();

            gameStatus = Status.QUIT;
        }    

        //the 0 is for xP since the player starts with 0 points
        Player userDefinedPlayer = 
            new Player(heroName, heroAttack, heroHealth, 0, heroMana);

        drawScreen();

        userDefinedPlayer.displayStats();

        pressEnterToContinue();

        return userDefinedPlayer;
    }
      
    //combat loop activiated by chance
    public static void runCombatLoop(Player hero)
    {
        drawScreen();

        //generate a random monster and display its stats
        Monster randomMonster = generateMonster();

        //equip monster with weapons or potions
        fillWithItems(randomMonster);

        //allows the hero to see their stats before making a decision on 
        //whether or not to enter combat with the randomMonster
        hero.displayStats();

        hero.displayItems();

        //allow hero to choose what to do
        combatStatus = hero.enterCombat();

        drawScreen();

        //while the combatStatus is true, the hero is alive, and the 
        //monster is alive, keep fighting
        while (combatStatus && hero.isAlive() && 
        	randomMonster.isAlive())
        {
            roundOfCombat(hero, randomMonster);
        }

        //if hero is alive and monster is not, get items from monster
        //and update hero's xP 
        if (hero.isAlive() && !randomMonster.isAlive())
        {
        	heroVictory(hero, randomMonster);
        }	

	}//end runCombatLoop   

    //runs one complete round of combat between hero and randomMonster
    //called by runCombatLoop()
    public static void roundOfCombat(Player hero, Monster randomMonster)
    {
        //allows the hero to choose whether to attack or take
        //a health/mana potion
        heroTurn(hero, randomMonster);

        //check to make sure the monster is still alive after being attacked
        if (randomMonster.isAlive())
        {
            //monster always attacks the hero. 
            monsterTurn(hero, randomMonster);
        }

        //Method also performs a health check on the hero and allows 
        //the hero to use a health potion before the next combat loop.
        lastBreath(hero);
    }

    //called by runCombatLoop() if hero is alive and randomMonster is dead
    //allows the hero to equip with any items the monster had, updates the hero's xp,
    //and checks to see if the hero leveled up.
    public static void heroVictory(Player hero, Monster randomMonster)
    {
        System.out.printf("You killed the %s!%n%n",
            randomMonster.getName());

        equipItems(hero, randomMonster);
        hero.updateXP(randomMonster.getXP());

        //check for levelUp
        hero.levelUp();

        pressEnterToContinue();
    }

	//generates a random instance of Monster
    //utility method called by runCombatLoop()
    public static Monster generateMonster()
    {
    	int monsterSelection = 1 + randomGenerator.nextInt(15);

    	Monster randomMonster;

    	if (monsterSelection == 1 || monsterSelection == 2
    		|| monsterSelection == 3)
    	{
    		randomMonster = new Goblin();
    		System.out.println("You encounter a Goblin!");
    		randomMonster.displayStats();
    	}	
    	else if (monsterSelection == 4 || monsterSelection == 5)
    	{
    		randomMonster = new Troll();
    		System.out.println("You encounter a Troll!");
    		randomMonster.displayStats();
    	}	
    	else if (monsterSelection == 6)
    	{
    		randomMonster = new Ork();
    		System.out.println("You encounter an Ork!");
    		randomMonster.displayStats();
    	}	
    	else if (monsterSelection == 7)
    	{
    		randomMonster = new Ogre();
    		System.out.println("You encounter an Ogre!");
    		randomMonster.displayStats();
    	}	
    	else //monsterSelection >= 8
    	{
    		randomMonster = new Gnome();
    		System.out.println("You encounter an Gnome!");
    		randomMonster.displayStats();
    	}

    	return randomMonster;
    }

	//utility method for roundOfCombat(). Processes hero's takeTurn() choice to 
	//useManaPotion, useHealthPotion, or attack
	public static void heroTurn(Player hero, Monster randomMonster)
	{
		randomMonster.displayStats();

		//user gets to choose next course of action
    	int heroChoice = hero.takeTurn();

    	if (heroChoice == 1)
    	{
    		System.out.printf("%nYou attack %s for %d damage!%n%n",
    			randomMonster.getName(), hero.attack());

    		randomMonster.updateHealth(-hero.attack());
    	}	
    	else if (heroChoice == 2)
    	{
    		hero.useManaPotion();
    	}	
    	else if (heroChoice == 3)
    	{
    		hero.useHealthPotion();
    	}
        else if (heroChoice == 4)
        {
            //checks for appropriate mana and casts the weaken spell on the monster
            magicAttack(hero, randomMonster);
        } 
        else if (heroChoice == 5)
        {
        	hero.castHealthSpell();
        }   

	}//end heroTurn method

    //method that checks for the appropriate mana level and then casts
    //a spell on the current monster that the hero is fighting
    //called by heroTurn()
    public static void magicAttack(Player hero, Monster randomMonster)
    {
       if (hero.getMana() >= 3)
            {
                System.out.printf("%nYou cast the weaken spell on the %s for %d damage!%n",
                    randomMonster.getName(), hero.castAttackSpell());

                randomMonster.updateHealth(-hero.castAttackSpell());
            }
            else
            {
                System.out.println("\nYou don't have enough mana to cast a spell!");
            }  
    }

	//utility method for runCombatLoop. Simulates the monster's turn
	public static void monsterTurn(Player hero, Monster randomMonster)
	{        	
    	//takeTurn() displays the monster's stats, tells how much damage is done
    	//to the hero, and returns that damage to the monsterAttack variable
    	int monsterAttack = randomMonster.takeTurn();

    	hero.updateHealth(monsterAttack);

        pressEnterToContinue();

    	drawScreen();

    }//end monsterTurn method

    //automatically uses a health potion if the hero's health is < 0
    //called by roundOfCombat()
    public static void lastBreath(Player hero)
    {
        if (hero.getHealth() <= 0)
        {
            //see if user has any health potions
            if (hero.hasHealthPotion())
            {
                System.out.println("You are gravely injured, " +
                    "but still have a health potion so you quickly take it.");
                hero.setHealth(0);
                hero.useHealthPotion();
            }   

            //if that still doesn't get their health above 0, they die!
            if (hero.getHealth() <= 0)
            {
                gameStatus = Status.DEAD;
            }   
        }
    }

    //displays win, loss, or quit message based on gameStatus. Called
    //by main method.
    public static void displayGameResult()
    {
        switch (gameStatus)
    	{
    		case WIN:
    			System.out.println("\n  You escape to safety with the Queen " +
                    "from Chargoth's lands. You have avenged your brother!");
    			break;
    		case LOSE:
    			System.out.println("\nYou leave Chargoth's lands a broken soul " +
                    "and wander the earth in sorrow for the rest of your days.");
    			break;
    		case QUIT:
    			System.out.println("\n                         " +
                    "Coward! You leave the castle in disgrace.");
    			break;
            case DEAD:
                System.out.println("\n                          " +
                    "            You are killed!");
                break;

    	}

    }//end displayGameResult method

    //called by main method. Randomly fills room with items
    public static void fillWithItems(Room currentRoom)
    {
    	//random number to choose which item to place in room
    	int randomNumber = 1 + randomGenerator.nextInt(150);

    	Weapon weapon = null;
    	HealthPotion healthPotion = null;
    	ManaPotion manaPotion = null;

    	if (randomNumber == 1)
    	{
    		currentRoom.setWeapon(weapon = new BroadSword());
    	}	
    	else if (randomNumber == 2 || randomNumber == 12)
    	{
    		currentRoom.setHealthPotion(healthPotion = new HealthPotion());
    	}	
    	else if (randomNumber == 3 || randomNumber == 13)
    	{
    		currentRoom.setManaPotion(manaPotion = new ManaPotion());
    	}	
    	else if (randomNumber == 4 || randomNumber == 14)
    	{
    		currentRoom.setWeapon(weapon = new CrossBow());
    	}	
    	else if (randomNumber == 5)
    	{
    		currentRoom.setWeapon(weapon = new BattleAxe());
    	}
    	else if (randomNumber == 6 || randomNumber == 16)
    	{
    		currentRoom.setWeapon(weapon = new Dagger());
    	}	


    }//end fillWithItems method

    //overloaded version, used to equip the randomMonster with items.
    //called by runCombatLoop. You have a better chance of getting an item 
    //off a a monster than out of a room because it makes sense to be rewarded
    //for killing one in most cases!
    public static void fillWithItems(Monster randomMonster)
    {
    	int randomNumber = 1 + randomGenerator.nextInt(45);

    	Weapon weapon = null;
    	HealthPotion healthPotion = null;
    	ManaPotion manaPotion = null;

    	if (randomNumber == 1)
    	{
    		randomMonster.setWeapon(weapon = new BroadSword());
    	}
    	else if (randomNumber == 2 || randomNumber == 12)
    	{
    		randomMonster.setHealthPotion(healthPotion = new HealthPotion());
    	}	
    	else if (randomNumber == 3 || randomNumber == 13)
    	{
    		randomMonster.setManaPotion(manaPotion = new ManaPotion());
    	}	
    	else if (randomNumber == 4 || randomNumber == 14)
    	{
    		randomMonster.setWeapon(weapon = new CrossBow());
    	}	
    	else if (randomNumber == 5)
    	{
    		randomMonster.setWeapon(weapon = new BattleAxe());
    	}	
    	else if (randomNumber == 6 || randomNumber == 16)
    	{
    		randomMonster.setWeapon(weapon = new Dagger());
    	}	

    }

    //called by main method, allows user to choose items from the room
    public static void equipItems(Player hero, Room currentRoom)
    {
    	//checks if any items are in the current room
    	if (currentRoom.hasItems())
    	{
    		//if there are items, check to see if they are weapons
    		if (currentRoom.hasWeapon())
    		{
    			System.out.printf("There is a %s in this room.%n" +
    				"It's attack power is %d%n" +
    				"Do you want to take it?%n" +
    				"Enter y/n: ",
    				currentRoom.getWeapon().getName(), 
    				currentRoom.getWeapon().getAttackDamage());
    			String userChoice = input.next();
    			input.nextLine();//consumes the newline character

    			System.out.println();

    			//if the user wants the weapon, equip hero with it
    			if (userChoice.equals("y"))
    			{
    				hero.setWeapon(currentRoom.getWeapon());

    				//make sure the currentRoom no longer holds the weapon
    				currentRoom.setWeapon(null);
    			}	
    			//no need for an else condition
    		}

    		//if there are items, check to see if they are potions
    		else if (currentRoom.hasPotion())
    		{
    			if (currentRoom.hasHealthPotion())
    			{	
	    			System.out.printf("There is a %s in this room.%n" +
	    				"Do you want to take it?%n" +
	    				"Enter y/n: ",
	    				currentRoom.getHealthPotion().getName());
	    			String userChoice = input.next();
	    			input.nextLine();//consumes the newline character

	    			System.out.println();

	    			//if the user wants the potion, equip hero with it
	    			if (userChoice.equals("y"))
	    			{
	    				hero.setHealthPotion(currentRoom.getHealthPotion());

	    				//make sure the currentRoom no longer holds the potion
	    				currentRoom.setHealthPotion(null);
	    			}	
	    			//no need for an else condition
	    		}	
	    		else if (currentRoom.hasManaPotion())
	    		{
	    			System.out.printf("There is a %s in this room.%n" +
	    				"Do you want to take it?%n" +
	    				"Enter y/n: ",
	    				currentRoom.getManaPotion().getName());
	    			String userChoice = input.next();
	    			input.nextLine();//consumes the newline character

	    			System.out.println();

	    			//if the user wants the potion, equip hero with it
	    			if (userChoice.equals("y"))
	    			{
	    				hero.setManaPotion(currentRoom.getManaPotion());

	    				//make sure the currentRoom no longer holds the potion
	    				currentRoom.setManaPotion(null);
	    			}	
	    		}	
    		}	
    	}	

    }//end equipItems method

    //overloaded. called by heroVictory() only if player is alive and
    //monster is dead. Allows player to equip with monster's items.
    public static void equipItems(Player hero, Monster randomMonster)
    {
    	//checks if any items are in the current room
    	if (randomMonster.hasItems())
    	{
    		//if there are items, check to see if they are weapons
    		if (randomMonster.hasWeapon())
    		{
    			System.out.printf("This %s had a %s.%n" +
    				"Its attack power is %d%n" +
    				"Do you want to take it?%n" +
    				"Enter y/n: ",
    				randomMonster.getName(),
    				randomMonster.getWeapon().getName(),
    				randomMonster.getWeapon().getAttackDamage());
    			String userChoice = input.next();
    			input.nextLine();//consumes the newline character

    			System.out.println();

    			//if the user wants the weapon, equip hero with it
    			if (userChoice.equals("y"))
    			{
    				hero.setWeapon(randomMonster.getWeapon());
    			}	
    		}

    		//if there are items, check to see if they are potions
    		else if (randomMonster.hasPotion())
    		{
    			if (randomMonster.hasHealthPotion())
    			{	
	    			System.out.printf("This %s had a %s.%n" +
	    				"Do you want to take it?%n" +
	    				"Enter y/n: ",
	    				randomMonster.getName(),
	    				randomMonster.getHealthPotion().getName());
	    			String userChoice = input.next();
	    			input.nextLine();//consumes the newline character

	    			System.out.println();

	    			//if the user wants the potion, equip hero with it
	    			if (userChoice.equals("y"))
	    			{
	    				hero.setHealthPotion(randomMonster.getHealthPotion());
	    			}	
	    		}	
	    		else if (randomMonster.hasManaPotion())
	    		{
	    			System.out.printf("This %s had a %s.%n" +
	    				"Do you want to take it?%n" +
	    				"Enter y/n: ",
	    				randomMonster.getName(),
	    				randomMonster.getManaPotion().getName());
	    			String userChoice = input.next();
	    			input.nextLine();//consumes the newline character

	    			System.out.println();

	    			//if the user wants the potion, equip hero with it
	    			if (userChoice.equals("y"))
	    			{
	    				hero.setManaPotion(randomMonster.getManaPotion());
	    			}	
	    		}	
    		}	
    	}	

    }//end equipItems method

    //keeps the screen from getting cluttered by essenttially clearing it through
    //using a bunch of newline characters
    public static void drawScreen()
    {
    	System.out.printf("%n%n%n%n%n%n%n%n%n%n" +
    		"%n%n%n%n%n%n%n%n%n%n%n%n%n%n%n%n%n%n%n%n" +
    		"%n%n%n%n%n%n%n%n%n%n%n%n%n%n%n%n%n%n%n%n" +
    		"%n%n%n%n%n%n%n%n%n%n%n%n%n%n%n%n%n%n%n%n");
    }

    public static void lastBattle(Player hero)
    {
        System.out.print("You are in King Chargoth's presence.\n" +
            "Are you ready to fight him and avenge your brother?\n" +
            "Enter y/n: ");
        String userChoice = input.next();
        input.nextLine();

        System.out.println();

        if (userChoice.equals("y"))
        {
            KingChargoth kingChargoth = new KingChargoth();

            drawScreen();

            //while the hero is alive, and 
            //kingChargoth is alive, keep fighting
            while (hero.isAlive() && 
                kingChargoth.isAlive())
            {
                roundOfCombat(hero, kingChargoth);

                //if kingChargoth is dead, give the hero the goldenKey
                if (!kingChargoth.isAlive())
                {
                    hero.setGoldenKey(kingChargoth.getGoldenKey());
                    System.out.println("You have defeated King Chargoth!\n");
                }    
            }
        } 
        else
        {
        	return;
        }

        pressEnterToContinue();   

        drawScreen();
    }

    //assissts the drawScreen() by allowing the user to press a key before 
    //drawScreen() is called in instances where there is no other reason to 
    //prompt the user
    //***Found out how to do this on stackOverflow at
    //http://stackoverflow.com/questions/18281543/java-using-scanner-enter-key-pressed
    //kudos to Caleb Brinkman for the tips!
    public static void pressEnterToContinue()
    {
        Scanner scanner = new Scanner(System.in);

        System.out.println("Press enter to continue");

        String readString = scanner.nextLine();
    }

    public static void displayOpeningMessage()
    {
        System.out.println("You are a knight from the distant hills of Nordingham, " +
            "and you're mission\nis to rescue King Chargoth's Queen, Estrella, " +
            "from his castle. You're brother, Atrillius, was\none of King Chargoth's " +
            "best knights, but was recently killed in a\nsmall battle on the outksirts " +
            "of his kingdom. It's believed he was assassinated by his\ncomrades on " +
            "Chargoth's orders because of suspicions that he was engaged in an affair " +
            "with\nthe queen. Before his final battle, he caught wind of the plot " +
            "and sent his most trusted\nmessenger to you with this letter:\n\n");

        System.out.println("\"Brother,\n\nAs I saddle my ebony horse and prepare to enter " +
            "battle, I fear today will be my last on\nthis Earth. As you know, " +
            "Estrella and I have been in love for some time, and have been\nplotting " +
            "a way to make a safe escape from Chargoth's lands. We had planned to " +
            "escape this\nevening, but we've just received word that King Poltory's " +
            "Royal Guard may be patrolling\nthe outskirts of Chargoth's lands, so a " +
            "small group of Chargoth's best knights will be\ndispatched to intercept them " +
            "before they reach the castle grounds. I am to be the only\nadvance scout " +
            "for this mission, ,which means I will be alone and exposed in a dangerous" +
            "\narea. If something should happen to me, please, find a way to get Estrella " +
            "away from\nChargoth, for I'm sure she will not be able to hide her grief, " +
            "and he will know that she\nwas indeed unfaithful. Brother, her fate is in your " +
            "hands!\n\nFortes Fortuna Iuvat,\n('Fortune Favors the Brave')\n\nAtrillius\"\n\n");

        pressEnterToContinue();

        drawScreen();

        System.out.println("In order to make your way through the castle, you will be " +
            "disguised as a lowly servant.\nYou must be cautious because the castle is " +
            "filled with strange beasts that are loyal to\nKing Chargoth and his men and " +
            "they will pounce on any unusual presence in the castle.\nAlthough you are a " +
            "strong and worthy knight, you lack the experience neccessary to take\non such " +
            "an experienced warrior as King Chargoth, so you must gain weaponry, potions, " +
            "and\nvaluable fighting experience by entering combat with these beasts as " +
            "you make your way\nthrough the castle. Once you've gained enough experience " +
            "and weaponry, reveal yourself\nand confront King Chargoth as he is feasting " +
            "in the dining room. If you succeed in\nslaying him, use his golden key to " +
            "rescue the princess from the dungeon.\n\n");
    }

    public static void rescueQueen()
    {
        System.out.println("You notice a small staircase in the corner of the room " +
            "so you climb down. When you reach\nthe bottom of the staircase " +
            "you find Queen Estrella locked in a small cell. She is bound\nand " +
            "unconscious, so you quickly insert the golden key into the lock and " +
            "try to rescue her.\nOnce the lock is opened you are blinded by a dense " +
            "fog and a voice bellows out the\nfollowing riddle:\n\n" +
            "\"Time was when I was weapon and warrior; Now the young hero hoods " +
            "me with gold,\nand twisted silver. At times men kiss me. At times I " +
            "speak and summon to battle\nloyal companions. At times a courser bears " +
            "me o'er marchland. At times a ship\nbears me o'er the billows, brightly " +
            "adorned. At times a fair maiden fills me with breath;\nAt times hard and " +
            "heedless I lie on the board, bereft of beauty. At times I hang\nwinsome " +
            "on a wall, richly embellished, where revelers drink. At times a warrior\n" +
            "bears me on a horse, a battle adornment, and I swallow, bright-shining, " +
            "the breath from\nhis bosom. At times with my strains I summon the heroes " +
            "proudly to wine. At ties I win\nback spoil from the spoiler, with sounding " +
            "voice, put foemen to flight. Now ask what I'm called.\"\n");

        System.out.print("Type your answer: ");
        String userAnswer = input.next();
        input.nextLine();

        drawScreen();

        if (userAnswer.toLowerCase().equals("horn") || 
            userAnswer.toLowerCase().equals("trumpet"))
        {
            System.out.println("The fog begins to dissapate and you realize that " +
                "you've answered the riddle correctly. The\nqueen regains " +
                "consciousness and you quickly wisk her out of the castle.\n");

            pressEnterToContinue();

            drawScreen();

            gameStatus = Status.WIN;
        }    
        else
        {
            System.out.println("You begin coughing and choking and you hear the " +
                "walls crumbling around you. The voice in\nthe fog begins mocking " +
                "your ignorance and you realize that you have failed to guess the\n" +
                "correct answer to the riddle. You are blindly searching for the " +
                "walls of the cell to get\nyour bearings when suddenly you realize " +
                "that you're no longer inside the castle but have\nsomehow been " +
                "transported into the dense forrest outside of the castle walls. " +
                "As you look\ntowards where the castle should stand you see a " +
                "terrifying sight, the castle has\ncollapsed! The Queen and everyone " +
                "inside is buried under the rubble, and you hang your\nhead in shame " +
                "at your failure.\n");

            pressEnterToContinue();

            drawScreen();

            gameStatus = Status.LOSE;
        }    
    }

}//end Adventure_Game_Final class